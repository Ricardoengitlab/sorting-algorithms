function insertionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    var index = i;
    while (index > 0) {
      if (arr[index] < arr[index - 1]) {
        var tmp = arr[index - 1];
        arr[index - 1] = arr[index];
        arr[index] = tmp;
        index--;
      } else {
        break;
      }
    }
  }
  return arr;
}

// console.log(insertionSort([1, 2, 3, 3, 10, 0]));
